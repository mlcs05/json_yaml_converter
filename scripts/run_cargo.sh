#!/usr/bin/env bash

# this script tells cargo to 
# 1) download deps
# 2) compile deps

# additional directories are created as a result of this run
# - deps (dependency directories)
# - .cargo sub directories
# - bin (compiled binary)

# the first time this is ran, it will take a long time
# cargo build can detect what changes have occurred in the
# source files (state is kept in .cargo and deps directories)
# but subsequent runs will be quick because cargo detects
# what changes have been made and only compiles those
APPNAME=${PWD##*/}
DOCKERTAG=$APPNAME:cargo
echo $DOCKERTAG
IMAGE=$(docker build --quiet --file ./docker/Dockerfile-cargo --tag $DOCKERTAG .)
docker run --rm -v "$PWD":/opt $IMAGE
docker image rm $DOCKERTAG
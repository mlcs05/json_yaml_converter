#!/usr/bin/env bash
APPNAME=${PWD##*/}
DOCKERTAG=$APPNAME:latest0
echo $DOCKERTAG
IMAGE=$(docker build --quiet --file docker/Dockerfile-run --tag $DOCKERTAG .)

# if no arguments, then run and remove the image
if [[ -z "$1" ]]
then
    docker run --rm -v "$PWD":/opt $IMAGE
    # docker run --rm -v "$PWD":/opt $DOCKERTAG
    docker image rm $APPNAME:$DOCKERTAG
fi
#!/usr/bin/env bash

# the service name in the compose file
SERVICENAME=app
# the amount in seconds to delay for testing to happen
SLEEPSECONDS=30

# build the services found in the compose file and spint them
docker-compose -f docker/compose/docker-compose.yml up --detach --build

# need to sleep due to detach mode
sleep $SLEEPSECONDS

# attach to the container and dump the logs
docker-compose -f docker/compose/docker-compose.yml logs $SERVICENAME

# dispose the containers, images and volumes
docker-compose -f docker/compose/docker-compose.yml down --volumes --rmi all
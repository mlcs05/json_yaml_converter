extern crate hyper;

use hyper::{Body, Request, Response, Server};
use hyper::service::service_fn_ok;
use hyper::rt::{self, Future};

fn main() {
    let addr = ([127, 0, 0, 1], 8000).into();
    let server = Server::bind(&addr).serve( || {
            // This is the `Service` that will handle the connection.
            // `service_fn_ok` is a helper to convert a function that
            // returns a Response into a `Service`.
            service_fn_ok(move |_: Request<Body>| {
                let body: Body = Body::from("Hello World!");
                Response::new(body)
            })
        }).map_err(|e| 
            eprintln!("server error: {}", e)
        );

    println!("Listening on http://{}", addr);

    rt::run(server);
}
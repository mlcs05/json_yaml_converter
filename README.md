### Overview ###

### Build ###

Run this from the project directory:

```
sh scripts/run_cargo.sh
```

Re-run this to compile any new changes.
The Rust compiler will only include the changes you made - rather than recompiling everything.

### Execute ###

Launches the service:

```
sh scripts/run_compose.sh
```
